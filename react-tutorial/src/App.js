import React from "react";
import Table from "./Tables";

// class App extends Component{
//   render(){
//     return (
//       <div className="App">
//         <Table />
//       </div>
//     )
//   }
// }

const App = () =>{
  const characters = [
    {
      name: 'Charlie',
      job: 'Janitor',
    },
    {
      name: 'Mac',
      job: 'Bouncer',
    },
    {
      name: 'Dee',
      job: 'Aspring actress',
    },
    {
      name: 'Dennis',
      job: 'Bartender',
    },
  ]


  return(
    <div className="App">
      <Table 
      characterData = {characters} 
      abc ="true" />
    
    </div>
  )
}

export default App