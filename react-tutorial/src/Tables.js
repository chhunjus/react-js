import React from "react";

const TableHeaders = () => {
    return(
        <thead>
     <tr>
         <th>Name</th>
         <th>Job</th>
      </tr>
        </thead>
    )
}

const TableBodys = (props) =>{
    const rows = props.tableDatas.map((row, index) =>{
        return (
            <tr key={index}>
                <td>{row.name}</td>
                <td>{row.job}</td>
            </tr>
        )
    })
    return(
        <tbody>{rows}</tbody>
    )
}

const Table = (props) =>{
    const {characterData} = props
    return(
            <table>
                <TableHeaders />
                <TableBodys tableDatas={characterData}/>
            </table>
        )
}

export default Table;